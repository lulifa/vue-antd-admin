// 视图组件
const view = {
    tabs: () => import('@/layouts/tabs'),
    blank: () => import('@/layouts/BlankView'),
    page: () => import('@/layouts/PageView'),
    exp403: () => import('@/pages/exception/403'),
    exp404: () => import('@/pages/exception/404'),
    exp500: () => import('@/pages/exception/500'),
    login: () => import('@/pages/login/Login'),
    demo: () => import('@/pages/demo/Demo'),
    usercenter: () => import('@/pages/user/center/Index'),
    usersetting: () => import('@/pages/user/setting/Index'),
    contract: () => import('@/pages/crm/contract/Index'),
};

// 路由组件注册
const routerMap = {
    login: {
        authority: '*',
        path: '/login',
        component: view.login,
    },
    demo: {
        renderMenu: false,
        component: view.demo,
    },
    exp403: {
        authority: '*',
        name: 'exp403',
        path: '403',
        component: view.exp403,
    },
    exp404: {
        name: 'exp404',
        path: '404',
        component: view.exp404,
    },
    exp500: {
        name: 'exp500',
        path: '500',
        component: view.exp500,
    },
    root: {
        path: '/',
        name: '首页',
        redirect: '/login',
        component: view.tabs,
    },
    user: {
        name: '个人视图',
        icon: 'home',
        component: view.blank,
    },
    usercenter: {
        name: '个人中心',
        renderMenu: false,
        component: view.usercenter,
    },
    usersetting: {
        name: '个人设置',
        renderMenu: false,
        component: view.usersetting,
    },
    crm: {
        name: '商务管理',
        icon: 'dashboard',
        component: view.blank,
    },
    customer: {
        name: '客户管理',
        renderMenu: false,
        component: view.contract,
    },
    saleslead: {
        name: '商机管理',
        renderMenu: false,
        component: view.contract,
    },
    contract: {
        name: '合同管理',
        renderMenu: false,
        component: view.contract,
    },
    parent2: {
        name: '父级路由2',
        icon: 'form',
        component: view.page,
    },
    exception: {
        name: '异常页',
        icon: 'warning',
        component: view.blank,
    },
};
export default routerMap;
