const config = {
    400: {
        img: require('@/assets/common/400.png'),
        title: '403',
        desc: '无法连接到服务器，请检查网络配置！',
    },
    403: {
        img: require('@/assets/common/403.png'),
        title: '403',
        desc: '抱歉，你无权访问该页面',
    },
    404: {
        img: require('@/assets/common/404.png'),
        title: '404',
        desc: '抱歉，你访问的页面不存在或仍在开发中',
    },
    500: {
        img: require('@/assets/common/500.png'),
        title: '500',
        desc: '抱歉，服务器出错了',
    },
};

export default config;
