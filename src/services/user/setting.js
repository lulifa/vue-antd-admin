import { request, METHOD } from '@/utils/request';

// 用户基本信息
export async function getUserInfo(parameter) {
    return request('UserSetting/GetUserInfo', METHOD.POST, parameter);
}
// 保存用户基本信息
export async function saveUserInfo(parameter) {
    return request('UserSetting/SaveUserInfo', METHOD.POST, parameter);
}
// 获取用户密码
export async function getUserPassword(parameter) {
    return request('UserSetting/GetUserPassword', METHOD.POST, parameter);
}
// 获取用户密码
export async function saveUserPassword(parameter) {
    return request('UserSetting/SaveUserPassword', METHOD.POST, parameter);
}
