import { request, METHOD } from '@/utils/request';

// 列表
export async function getList(parameter) {
    return request('Contract/GetPagedList', METHOD.POST, parameter);
}
