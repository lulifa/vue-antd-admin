import { request, METHOD, removeAuthorization } from '@/utils/request';

// 登录服务
export async function login(parameter) {
    return request('Login/Login', METHOD.POST, parameter);
}

// 异步获取路由
export async function getRoutesConfig(parameter) {
    return request('Menu/GetRoutesConfig', METHOD.GET, parameter);
}

/**
 * 退出登录
 */
export function logout() {
    localStorage.removeItem(process.env.VUE_APP_ROUTES_KEY);
    localStorage.removeItem(process.env.VUE_APP_PERMISSIONS_KEY);
    localStorage.removeItem(process.env.VUE_APP_ROLES_KEY);
    removeAuthorization();
}
